# from typing import Any
from django import forms
from django.contrib.auth.models import User

#Django form
from django.contrib.auth.forms import UserCreationForm, UsernameField

#оптимизированная Django form
class MyUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields =[
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
        widgets={
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in ['password1', 'password2']:
            self.fields[field_name].widget.attrs.update({'class': 'form-control'})


# #Django form
# class MyUserCreationForm(UserCreationForm):
#     class Meta:
#         model = User
#         fields =[
#             'username',
#             'first_name',
#             'last_name',
#             'email',
#             'password1',
#             'password2'
#         ]
#         field_classes = {'username': UsernameField}

# # custom form
# class MyUserCreationForm(forms.ModelForm):
#     password = forms.CharField(
#         label='Password',
#         required=True,
#         strip=False,
#         widget=forms.PasswordInput()
#     )
#     password_confirm = forms.CharField(
#         label='Confirm password',
#         required=True,
#         strip=False,
#         widget=forms.PasswordInput()
#     )

#     def clean(self):
#         cleaned_data = super().clean()
#         password = cleaned_data.get('password')
#         password_confirm = cleaned_data.get('password_confirm')

#         if password and password_confirm and password != password_confirm:
#             raise forms.ValidationError('Passwords do not match!')
        
#     def save(self, commit=True):
#         user = super().save(commit=False)
#         user.set_password(self.cleaned_data['password'])
#         if commit:
#             user.save()
#         return user
    
#     class Meta:
#         model = User
#         fields = [
#             'username',
#             'first_name',
#             'last_name',
#             'email',
#             'password',
#             'password_confirm'
#         ]