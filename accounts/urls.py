from django.urls import path
from accounts.views import(
    login_view,
    logout_view,
    RegisterView,
)

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', RegisterView.as_view(), name='register'), 
]

# #для использования Джанговских форм авторизации
# from django.contrib.auth.views import LoginView, LogoutView

# #для использования Джанговских форм авторизации
# urlpatterns = [
#     path('login/', LoginView.as_view(), name='login'),
#     path('logout', LogoutView.as_view(), name='logout'), 
# ]