from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout

from django.views.generic import CreateView
from django.contrib.auth.models import User

from .forms import MyUserCreationForm

def login_view(request):
    context = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('home_page')
        else:
            context['has_error'] = True
    return render(request, 'accounts/login.html', context=context)

def logout_view(request):
    logout(request)
    next = request.GET.get('next')
    if next:
        return redirect(next)
    return redirect('login')

#регистрация через класс - джанговский дженерик CreateView
class RegisterView(CreateView):
    model = User
    template_name = 'accounts/register.html'
    form_class = MyUserCreationForm

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect(self.get_success_url())
    
    def get_success_url(self):
        next = self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('next')
        return next
    
#регистрация через кастом.функцию
# def register_view(request):
#     if request.method == 'POST':
#         form = MyUserCreationForm(data=request.POST)
#         if form.is_valid():
#             user = form.save()
#             login(request, user)
#             return redirect('home_page')
#     else:
#         form = MyUserCreationForm()
#     return render(request, 'accounts/register.html', context={'form': form})