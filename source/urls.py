from django.contrib import admin
from django.urls import include, path
from .views import (
    IndexView
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='home_page'),
    path('task/', include('todo_list.urls.task_urls')),
    path('accounts/', include('accounts.urls')),
]