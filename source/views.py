from django.views.generic import TemplateView 

# с дженериком TemplateView (class base)
class IndexView(TemplateView):
    template_name = 'index.html'