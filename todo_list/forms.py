from django import forms
from django.forms import widgets
from .models import Task

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'}),
            'description': forms.Textarea(attrs={
                'class': 'form-control', 
                'rows': 3}),
            'status': forms.Select(attrs={
                'class': 'form-select',
                'placeholder': 'Status'})
        }

# class TaskForm(forms.Form):
#     title = forms.CharField(
#         max_length=200, 
#         required=True, 
#         label="Title*",
#         widget=widgets.TextInput(attrs={
#             'class': 'form-control'
#             })
#         )
#     description = forms.CharField(
#         max_length=3000, 
#         required=True, 
#         label="Description*", 
#         widget=widgets.Textarea(attrs={
#             'class': 'form-control',
#             'rows': 3
#             })
#         )
#     status = forms.ChoiceField(
#         required=False,
#         label="Status",
#         widget=widgets.Select(attrs={
#             'class': 'form-select',
#             'placeholder': 'Status'
#             }),
#         choices=Task.STATUS_CHOICES
#         )    