# Generated by Django 5.0.2 on 2024-03-13 11:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Название задачи')),
                ('description', models.TextField(max_length=3000, verbose_name='Описание задачи')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата изменения')),
                ('status', models.CharField(choices=[('waiting', 'Waiting'), ('in_progress', 'In Progress'), ('done', 'Done')], default='waiting', max_length=20, null=True, verbose_name='Статус задачи')),
            ],
        ),
    ]
