from django.db import models
# from django.conf import settings
# from django.contrib.auth import get_user_model

class Task(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Название задачи')
    description = models.TextField(max_length=3000, null=False, blank=False, verbose_name='Описание задачи')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    STATUS_CHOICES =(
        ('waiting', 'Waiting'),
        ('in_progress', 'In Progress'),
        ('done', 'Done'),
    )
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='waiting', null=True, verbose_name='Статус задачи')


    def __str__(self):
        return f"#{self.pk} - {self.title}"