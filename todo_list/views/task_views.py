from django.http import HttpRequest
from django.http.response import HttpResponse as HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from ..models import Task
from ..forms import TaskForm
from django.core.paginator import Paginator

from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin

from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy

class TaskListView(ListView):
    template_name = 'tasks/list.html'
    model = Task
    context_object_name = 'tasks'
    ordering = ['title']
    paginate_by = 3
    paginate_orphans = 1

class TaskDetailView(DetailView):
    template_name = 'tasks/detail.html'
    model = Task
    context_object_name = 'task'

class TaskCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'tasks/create.html'
    model = Task
    form_class = TaskForm
    permission_required = 'todo_list.add_task'

    def get_success_url(self):
        return reverse_lazy('task_list')
    
class TaskUpdateView(PermissionRequiredMixin,UpdateView):
    template_name = 'tasks/update.html'
    model = Task
    form_class = TaskForm
    context_object_name = 'task'
    permission_required = 'todo_list.change_task'

    def get_success_url(self):
        return reverse('task_detail', kwargs={'pk': self.object.pk})
    
class TaskDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'tasks/delete.html'
    model = Task
    context_object_name = 'task'
    success_url = reverse_lazy('task_list')
    permission_required = 'todo_list.delete_task'


# def index_view(request):
#     return render(request, 'index.html')

# def task_detail_view(request, *args, **kwargs):
#     task = get_object_or_404(Task, pk=kwargs.get('pk'))
#     return render(request, 'tasks/detail.html',
#                   context={'task': task})

# def task_list_view(request):
#     sort_by = request.GET.get('sort_by', 'created_at')
#     tasks = Task.objects.all().order_by(sort_by)
#     items_per_page = 5
#     paginator = Paginator(tasks, items_per_page)
#     page_number = request.GET.get('page', 1)
#     page = paginator.get_page(page_number)

#     return render(request, 'tasks/list.html', context={'page': page, 'sort_by': sort_by})

# @login_required
# def task_create_view(request):
#     if request.method == 'GET':
#         form = TaskForm()
#         return render(request, 'tasks/create.html', context={'form': form})
#     elif request.method == 'POST':
#         form = TaskForm(request.POST)
#         if form.is_valid():
#             new_task = Task.objects.create(
#                 title=form.cleaned_data['title'],
#                 description=form.cleaned_data['description'],
#                 status=form.cleaned_data['status']
#             )
#             return redirect('task_list')
#         else:
#             return render(request, 'tasks/create.html', context={
#                 'form': form
#             })  

# @login_required
# def task_update_view(request, *args, **kwargs):
#     task = get_object_or_404(Task, pk=kwargs.get('pk'))

#     if request.method == 'GET':
#         form = TaskForm(initial={
#             'title': task.title,
#             'description': task.description,
#             'status': task.status
#         })
#         return render(request, 'tasks/update.html', context={'form': form, 'task': task})
#     elif request.method == 'POST':
#         form = TaskForm(data=request.POST)
#         if form.is_valid():
#             task.title = form.cleaned_data.get('title')
#             task.description = form.cleaned_data.get('description')
#             task.status = form.cleaned_data.get('status')
#             task.save()
#             return redirect('task_detail', pk=task.pk)
#         else:
#             return render(request, 'tasks/update.html', context={'form': form, 'task': task})

# @login_required
# def task_delete_view(request, *args, **kwargs):
#     task = get_object_or_404(Task, pk=kwargs.get('pk'))
#     if request.method =='GET':
#         return render(request, 'tasks/delete.html', context={'task': task})
#     elif request.method == 'POST':
#         task.delete()
#         return redirect('task_list')